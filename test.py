#!/usr/bin/env python2.7

class MyClass:
	def __init__(self):
		print "This is the constructor."
		print r"This print statement is indented twice using two tabs characters (\t, 0x09)"
		print "See https://gitlab.com/gitlab-org/gitlab-ce/issues/3220"

obj = MyClass()
